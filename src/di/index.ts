// tslint:disable:max-line-length
import { Validator } from "le-validator";
import { PartialResponsify } from "partial-responsify";
import { Reservation, Review, NoRoom, CheckIn, CheckOut } from "../entity";
import { IDI } from "../interface";
import
{
    Config,
    Helper,
    AppTypeOrm
} from "../lib";
import { ReservationService, NoRoomService, CheckInService, CheckOutService, ReviewService } from "../services";

export const getAppTypeOrm = async (config: Config): Promise<AppTypeOrm> =>
{
    return new AppTypeOrm(await AppTypeOrm.getConnectionManager({
        database: config.dbDatabase,
        entities: [
            Reservation,
            Review,
            NoRoom,
            CheckIn,
            CheckOut
        ],
        host: config.dbHost,
        logging: config.dbLogging,
        password: config.dbPassword,
        type: config.dbType,
        username: config.dbUsername,
    }));
};

export const getDiComponent = async (config: Config): Promise<IDI> =>
{
    const di: IDI = {
        appTypeOrm: await getAppTypeOrm(config),
        config,
        helper: new Helper(),
        partialResponsify: new PartialResponsify(),
        validator: new Validator(),
        reservationService: null,
        noRoomService: null,
        checkInService: null,
        checkOutService: null,
        reviewService: null
    };

    di.reservationService = new ReservationService(di);
    di.noRoomService = new NoRoomService(di);
    di.checkInService = new CheckInService(di);
    di.checkOutService = new CheckOutService(di);
    di.reviewService = new ReviewService(di);
    return di;
};
