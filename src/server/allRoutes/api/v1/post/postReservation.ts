import { IRouterContext } from "koa-router";
import { ResponseFormat } from "partial-responsify";
import { IAllRoute, IDI, IOpenApiRoute } from "../../../../../interface";
import { BodyFormat, ValidatorGetParam } from "../../../../../lib";
// import {
//     commonHeaderParams,
// } from "../../../../../scripts";
import { PageableGetParam } from "../../../../../lib/appValidator";
import { Reservation } from "../../../../../entity";

const path = "/v1/reservation";
const method = "POST";
const get: ValidatorGetParam[] = [];
const body: BodyFormat = {
    required: true,
    type: "object",
    swagger: {
        description: "",
        example: {
            author: "",
            description: "",
        },
    },
    fields: {
        name: {
            type: "string",
            required: true
        },
        phone: {
            type: "number",
            required: true
        },
        guest: {
            type: "number",
            required: true
        },
        timeIn: {
            type: "string",
            required: true
        },
        timeOut: {
            type: "string",
            required: true
        },
    },
};
// const headerParams: ValidatorHeaderParam[] = commonHeaderParams;
const responseFormat: ResponseFormat = {
    items: {
        fields: {
            Code: {
                type: "any",
            },
            Desc: {
                type: "string",
            },
            ID: {
                type: "string",
            },
            LastUpdate: {
                type: "string",
            },
        },
        type: "object",
    },
    type: "array",
};
const successResponseFormat: ResponseFormat = {
    fields: {
        data: responseFormat,
    },
    type: "object",
};
const func = async (ctx: IRouterContext): Promise<void> =>
{
    const di: IDI = ctx.state.di;

    // Validate query params
    di.validator.processQuery<PageableGetParam>(get, ctx.request.query);

    // Validate body
    const requestBody = di.validator.processBody<Reservation>(body, ctx.request.body);

    // If you need to validate the headers, uncomment this
    // const headers = di.validator.processHeader<ICommonHeaderParamsProcessed>(headerParams, ctx.request.headers);

    ctx.status = 200;
    ctx.body = await di.reservationService.create(requestBody);
};
export const openapiPostReservation: IOpenApiRoute = {
    get,
    // headerParams,
    method,
    path,
    successResponseFormat,
    tags: ["masterdata"],
};
export const postReservation: IAllRoute = {
    func,
    method,
    path,
};
