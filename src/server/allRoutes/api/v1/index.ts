import { IAllRoute, IOpenApiRoute } from "../../../../interface";

// ROUTE CONTROLER IMPORTS
// GET ROUTES
import { openapiGetReservation, getReservation } from './get/getReservation';
import { openapiGetReservationById, getReservationById } from './get/getReservationById';
import { openapiGetRoom, getRoom } from './get/getAllRoom';
import { openapiGetCheckIn, getCheckIn } from './get/getAllCheckIn';
import { openapiGetCheckOut, getCheckOut } from './get/getAllCheckOut';
import { openapiGetReview, getReview } from './get/getAllReview';


// POST ROUTES
import { openapiPostReservation, postReservation } from "./post/postReservation";
import { openapiPostRoom, postRoom } from "./post/postNoRoom";
import { openapiPostCheckIn, postCheckIn } from "./post/postCheckIn";
import { openapiPostCheckOut, postCheckOut } from "./post/postCheckOut";
import { openapiPostReview, postReview } from "./post/postReview";

// PUT ROUTES
import { openapiPutReservation, putReservation } from "./put/putReservation";
import { openapiPutCheckIn, putCheckIn } from "./put/putCheckIn";

// DELETE ROUTES
import { openapiDeleteReservationById, deleteReservationById } from './delete/deleteReservation';


interface IRouteTuple
{
    0: IOpenApiRoute;
    1: IAllRoute;
}

// REGISTER THE ROUTE CONTROLLER HERE
const routes: IRouteTuple[] = [
    [openapiGetReservation, getReservation],
    [openapiGetReservationById, getReservationById],
    [openapiGetRoom, getRoom],
    [openapiGetCheckIn, getCheckIn],
    [openapiGetCheckOut, getCheckOut],
    [openapiGetReview, getReview],

    // POST ROUTES
    [openapiPostReservation, postReservation],
    [openapiPostRoom, postRoom],
    [openapiPostCheckIn, postCheckIn],
    [openapiPostCheckOut, postCheckOut],
    [openapiPostReview, postReview],

    // PUT ROUTES
    [openapiPutReservation, putReservation],
    [openapiPutCheckIn, putCheckIn],

    // DELETE ROUTES
    [openapiDeleteReservationById, deleteReservationById]

];
const tmpBaseOpenApiRoutes: IOpenApiRoute[] = [];
const tmpBaseRoutes: IAllRoute[] = [];
for (const route of routes)
{
    if (!route[1].path.startsWith("/v1/") || !route[0].path.startsWith("/v1/"))
    {
        // tslint:disable-next-line: no-console
        console.log(route[1]);
        throw new Error("path should start with v1");
    }
    tmpBaseOpenApiRoutes.push(route[0]);
    tmpBaseRoutes.push(route[1]);
}

// seperate the route to openapi and application route, future easier to extend
export const baseOpenApiRoutes: IOpenApiRoute[] = tmpBaseOpenApiRoutes;
export const baseRoutes: IAllRoute[] = tmpBaseRoutes;
