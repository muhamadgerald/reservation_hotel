import { NoRoom } from "../entity";
import { IDI } from "../interface";
import { Repository } from "typeorm";
import { PageableGetParam } from "../lib/appValidator";
import { Pagination } from "../lib/helper/pagination";
import { DataNotFoundError } from "../error";

export class NoRoomService
{
    private static noRoomRepository: Repository<NoRoom> = null;

    constructor(di: IDI)
    {
        if (NoRoomService.noRoomRepository === null)
        {
            NoRoomService.noRoomRepository = di.appTypeOrm.getConnection().getRepository(NoRoom);
        }

    }

    public async findAll(query: PageableGetParam): Promise<NoRoom[]>
    {
        return await NoRoomService.noRoomRepository.find(Pagination.paginateOption(query));
    }

    public async findById(room: number): Promise<NoRoom>
    {
        return await NoRoomService.noRoomRepository
            .findOne({
                where: { room },
            });
    }

    public async create(data: NoRoom): Promise<NoRoom>
    {

        const noRoom = new NoRoom();
        noRoom.room = data.room;
        noRoom.fill = false;

        try
        {
            return await NoRoomService.noRoomRepository.save(noRoom);
        } catch (e)
        {
            console.log(e);
            throw new Error(e);
        }
    }

    public async changeStatus(noRoom: any, typeCheck: any): Promise<NoRoom>
    {
        const room = await this.findById(noRoom.room);
        if (room == null)
        {
            throw new Error(`Room not found ${noRoom.room}`);
        }

        if (typeCheck === 'IN')
        {
            room.fill = true;
        } else
        {
            room.fill = false;
        }
        return await NoRoomService.noRoomRepository.save(room);

    }


    public async delete(id: number): Promise<NoRoom>
    {
        const noRoom: NoRoom = await this.findById(id);
        if (noRoom == null)
        {
            throw new DataNotFoundError(`Room not found: ${id}`);
        }
        await NoRoomService.noRoomRepository.save(noRoom);
        return noRoom;
    }
}