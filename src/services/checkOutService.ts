import { CheckOut } from "../entity";
import { IDI } from "../interface";
import { Repository } from "typeorm";
import { PageableGetParam } from "../lib/appValidator";
import { Pagination } from "../lib/helper/pagination";
import { DataNotFoundError } from "../error";
import dayjs = require("dayjs");
import { NoRoomService } from "./noRoomService";
import { CheckInService } from "./checkInService";

export class CheckOutService
{
    private static checkOutRepository: Repository<CheckOut> = null;
    private static roomRepository: NoRoomService = null;
    private static checkInRepository: CheckInService = null;

    constructor(di: IDI)
    {
        if (CheckOutService.checkOutRepository === null)
        {
            CheckOutService.checkOutRepository = di.appTypeOrm.getConnection().getRepository(CheckOut);
        }

        if (CheckOutService.roomRepository === null)
        {
            CheckOutService.roomRepository = new NoRoomService(di);
        }
        if (CheckOutService.checkInRepository === null)
        {
            CheckOutService.checkInRepository = new CheckInService(di);
        }
    }

    public async findAll(query: PageableGetParam): Promise<CheckOut[]>
    {
        return await CheckOutService.checkOutRepository.find(Pagination.paginateOption(query, { relations: ['checkIn', 'noRoom'] }));
    }

    public async findById(id: number): Promise<CheckOut>
    {
        return await CheckOutService.checkOutRepository
            .findOne({
                where: { id },
            });
    }

    public async create(data: any): Promise<CheckOut>
    {
        const checkIn = await CheckOutService.checkInRepository.findById(data.checkIn);
        if (!checkIn || checkIn.checkOut)
        {
            throw new DataNotFoundError(`Checkin not found: ${data.checkIn}`);
        }

        console.log(checkIn.noRoom.room);


        const checkOut = new CheckOut();
        checkOut.checkIn = data.checkIn;
        checkOut.timeOut = dayjs().toString();
        checkOut.noRoom = checkIn.noRoom.room;
        const saveCheckOut = await CheckOutService.checkOutRepository.save(checkOut);
        await CheckOutService.roomRepository.changeStatus(checkIn.noRoom.room, 'OUT');
        await CheckOutService.checkInRepository.changeStatus(data.checkIn);
        try
        {
            return await saveCheckOut;
        } catch (e)
        {
            console.log(e);
            throw new Error(e);
        }
    }

    public async delete(id: number): Promise<CheckOut>
    {
        const checkOut: CheckOut = await this.findById(id);
        if (checkOut == null)
        {
            throw new DataNotFoundError(`Checkout not found: ${id}`);
        }
        await CheckOutService.checkOutRepository.save(checkOut);
        return checkOut;
    }
}