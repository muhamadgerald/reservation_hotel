import { CheckIn } from "../entity";
import { IDI } from "../interface";
import { Repository } from "typeorm";
import { PageableGetParam } from "../lib/appValidator";
import { Pagination } from "../lib/helper/pagination";
import { DataNotFoundError } from "../error";
import dayjs = require("dayjs");
import { NoRoomService } from "./noRoomService";

export class CheckInService
{
    private static checkInRepository: Repository<CheckIn> = null;
    private static roomRepository: NoRoomService = null;

    constructor(di: IDI)
    {
        if (CheckInService.checkInRepository === null)
        {
            CheckInService.checkInRepository = di.appTypeOrm.getConnection().getRepository(CheckIn);
        }

        if (CheckInService.roomRepository === null)
        {
            CheckInService.roomRepository = new NoRoomService(di);
        }
    }

    public async findAll(query: PageableGetParam): Promise<CheckIn[]>
    {
        return await CheckInService.checkInRepository.find(Pagination.paginateOption(query, { relations: ['reservation', 'noRoom'] }));
    }

    public async findById(id: number): Promise<CheckIn>
    {
        return await CheckInService.checkInRepository
            .findOne({
                where: { id },
                relations: ['reservation', 'noRoom']

            });
    }

    public async create(data: CheckIn): Promise<CheckIn>
    {
        const dataNoRoom: any = data.noRoom;
        const checkRoom = await CheckInService.roomRepository.findById(dataNoRoom);
        if (!checkRoom)
        {
            throw new DataNotFoundError(`Room not found: ${data.noRoom}`);
        }

        if (checkRoom.fill)
        {
            throw new Error(`Room is already fill`);
        }

        const checkIn = new CheckIn();
        checkIn.noRoom = data.noRoom;
        checkIn.reservation = data.reservation;
        checkIn.timeIn = dayjs().toString();
        checkIn.checkOut = false;
        const saveCheckIn = await CheckInService.checkInRepository.save(checkIn);
        await CheckInService.roomRepository.changeStatus(data.noRoom, 'IN');
        try
        {
            return await saveCheckIn;
        } catch (e)
        {
            console.log(e);
            throw new Error(e);
        }
    }

    public async changeStatus(changeStatus: CheckIn): Promise<CheckIn>
    {
        const changeStatusCheckOut = await this.findById(changeStatus.id);
        if (changeStatusCheckOut == null)
        {
            throw new Error(`Check In not found ${changeStatus.id}`);
        }

        changeStatusCheckOut.checkOut = true;
        return await CheckInService.checkInRepository.save(changeStatusCheckOut);

    }

    public async update(checkInUpdate: CheckIn): Promise<CheckIn>
    {
        const checkIn = await this.findById(checkInUpdate.id);
        if (checkIn == null)
        {
            throw new Error(`Checkin not found ${checkInUpdate.id}`);
        }

        if (checkInUpdate.noRoom !== null || checkInUpdate.noRoom !== undefined)
        {
            checkIn.noRoom = checkInUpdate.noRoom;
        }

        return await CheckInService.checkInRepository.save(checkIn);
    }




    public async delete(id: number): Promise<CheckIn>
    {
        const checkIn: CheckIn = await this.findById(id);
        if (checkIn == null)
        {
            throw new DataNotFoundError(`Checkin not found: ${id}`);
        }
        await CheckInService.checkInRepository.save(checkIn);
        return checkIn;
    }
}