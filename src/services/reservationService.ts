import { Reservation } from "../entity";
import { IDI } from "../interface";
import { Repository } from "typeorm";
import { PageableGetParam } from "../lib/appValidator";
import { Pagination } from "../lib/helper/pagination";
import { DataNotFoundError } from "../error";

export class ReservationService
{
    private static reservationRepository: Repository<Reservation> = null;

    constructor(di: IDI)
    {
        if (ReservationService.reservationRepository === null)
        {
            ReservationService.reservationRepository = di.appTypeOrm.getConnection().getRepository(Reservation);
        }

    }

    public async findAll(query: PageableGetParam): Promise<Reservation[]>
    {
        return await ReservationService.reservationRepository.find(Pagination.paginateOption(query));
    }

    public async findById(id: number): Promise<Reservation>
    {
        return await ReservationService.reservationRepository
            .findOne({
                where: { id },
            });
    }

    public async create(data: Reservation): Promise<Reservation>
    {

        const reservation = new Reservation();
        reservation.name = data.name;
        reservation.guest = data.guest;
        reservation.phone = data.phone;
        reservation.timeIn = data.timeIn;
        reservation.timeOut = data.timeOut;

        try
        {
            return await ReservationService.reservationRepository.save(reservation);
        } catch (e)
        {
            console.log(e);
            throw new Error(e);
        }
    }

    public async update(reservationUpdate: Reservation): Promise<Reservation>
    {
        const reservation = await this.findById(reservationUpdate.id);
        if (reservation == null)
        {
            throw new Error(`Reservation not found ${reservationUpdate.id}`);
        }

        if (reservationUpdate.name !== null || reservationUpdate.name !== undefined)
        {
            reservation.name = reservationUpdate.name;
        }

        if (reservationUpdate.phone !== null || reservationUpdate.phone !== undefined)
        {
            reservation.phone = reservationUpdate.phone;
        }
        if (reservationUpdate.guest !== null || reservationUpdate.guest !== undefined)
        {
            reservation.guest = reservationUpdate.guest;
        }
        if (reservationUpdate.timeIn !== null || reservationUpdate.timeIn !== undefined)
        {
            reservation.timeIn = reservationUpdate.timeIn;
        }
        if (reservationUpdate.timeOut !== null || reservationUpdate.timeOut !== undefined)
        {
            reservation.timeOut = reservationUpdate.timeOut;
        }

        return await ReservationService.reservationRepository.save(reservation);
    }

    public async delete(id: number): Promise<Reservation>
    {
        const reservation: Reservation = await this.findById(id);
        if (reservation == null)
        {
            throw new DataNotFoundError(`Reservation not found: ${id}`);
        }
        await ReservationService.reservationRepository.save(reservation);
        return reservation;
    }
}