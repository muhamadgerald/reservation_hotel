import { Review } from "../entity";
import { IDI } from "../interface";
import { Repository } from "typeorm";
import { PageableGetParam } from "../lib/appValidator";
import { Pagination } from "../lib/helper/pagination";
import { DataNotFoundError } from "../error";
import { ReservationService } from "./reservationService";

export class ReviewService
{
    private static reviewRepository: Repository<Review> = null;
    private static reservationRepository: ReservationService = null;

    constructor(di: IDI)
    {
        if (ReviewService.reviewRepository === null)
        {
            ReviewService.reviewRepository = di.appTypeOrm.getConnection().getRepository(Review);
        }

        if (ReviewService.reservationRepository === null)
        {
            ReviewService.reservationRepository = new ReservationService(di);
        }
    }

    public async findAll(query: PageableGetParam): Promise<Review[]>
    {
        return await ReviewService.reviewRepository.find(Pagination.paginateOption(query, {
            relations: ['reservation']
        }));
    }

    public async findById(id: number): Promise<Review>
    {
        return await ReviewService.reviewRepository
            .findOne({
                where: { id },
                relations: ['reservation']
            });
    }

    public async create(data: Review): Promise<Review>
    {

        const checkReservation = await ReviewService.reservationRepository.findById(data.reservation);
        if (!checkReservation)
        {
            throw new DataNotFoundError(`Reservation not found: ${data.reservation}`);
        }

        const review = new Review();
        review.name = checkReservation.name;
        review.reservation = data.reservation;
        review.star = data.star;

        try
        {
            return await ReviewService.reviewRepository.save(review);
        } catch (e)
        {
            console.log(e);
            throw new Error(e);
        }
    }

    public async delete(id: number): Promise<Review>
    {
        const review: Review = await this.findById(id);
        if (review == null)
        {
            throw new DataNotFoundError(`Review not found: ${id}`);
        }
        await ReviewService.reviewRepository.save(review);
        return review;
    }
}