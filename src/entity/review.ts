import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from 'typeorm';
import { Reservation } from './reservation';

@Entity({
    name: 'review'
})
export class Review
{
    @PrimaryGeneratedColumn({ name: 'id' })
    public id: number;

    @Column({ name: 'star' })
    public star: number;

    @Column({ name: 'name' })
    public name: string;

    @ManyToOne(() => Reservation, reservation => reservation.id)
    @JoinColumn({ name: 'id_reservation' })
    public reservation: number;

}