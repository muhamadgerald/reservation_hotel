import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity({
    name: 'reservation'
})
export class Reservation
{
    @PrimaryGeneratedColumn({ name: 'id' })
    public id: number;

    @Column({ name: 'time_in', type: 'datetime' })
    public timeIn: Date;

    @Column({ name: 'time_out', type: 'datetime' })
    public timeOut: Date;

    @Column({ name: 'guest' })
    public guest: number;

    @Column({ name: 'name' })
    public name: string;

    @Column({ name: 'phone' })
    public phone: number;

}