import { Entity, Column, PrimaryColumn } from 'typeorm';

@Entity({
    name: 'no_room'
})
export class NoRoom
{
    @PrimaryColumn({ name: 'room' })
    public room: number;

    @Column({ name: 'fill' })
    public fill: boolean;

}