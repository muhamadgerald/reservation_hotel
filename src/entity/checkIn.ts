import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from 'typeorm';
import { NoRoom } from './noRoom';
import { Reservation } from './reservation';

@Entity({
    name: 'check_in'
})
export class CheckIn
{
    @PrimaryGeneratedColumn({ name: 'id' })
    public id: number;

    @Column({ name: 'time_in', type: 'timestamp' })
    public timeIn: string;

    @ManyToOne(() => Reservation, reservation => reservation.id)
    @JoinColumn({ name: 'reservation_id' })
    public reservation: Reservation;

    @ManyToOne(() => NoRoom, noRoom => noRoom.room)
    @JoinColumn({ name: 'no_room' })
    public noRoom: NoRoom;

    @Column({ name: 'check_out' })
    public checkOut: boolean;
}