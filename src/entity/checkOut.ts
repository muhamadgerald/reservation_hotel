import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from 'typeorm';
import { CheckIn } from './checkIn';
import { NoRoom } from './noRoom';

@Entity({
    name: 'check_out'
})
export class CheckOut
{
    @PrimaryGeneratedColumn({ name: 'id' })
    public id: number;

    @Column({ name: 'time_out', type: 'timestamp' })
    public timeOut: string;

    @ManyToOne(() => CheckIn, checkIn => checkIn.id)
    @JoinColumn({ name: 'checkin_id' })
    public checkIn: CheckIn;

    @ManyToOne(() => NoRoom, noRoom => noRoom.room)
    @JoinColumn({ name: 'no_room' })
    public noRoom: any;

}